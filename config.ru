require 'rubygems'
require 'bundler'

Bundler.require

require './lib/cow_app'
run CowApp
