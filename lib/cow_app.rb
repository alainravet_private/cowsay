require 'sinatra'
require_relative 'cowsay'

class CowApp < Sinatra::Base
  get "/" do
    content_type :text
    message = params[:message] || "hello"
    Cowsay::Cow.new.say(message)
  end
end
