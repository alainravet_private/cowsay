require 'pathname'
module Cowsay
  class Cow
    def say(message)
      perl_path = '/usr/bin/perl'
      exec_path = Pathname(__FILE__).dirname.to_s + "/../bin/cowsay"
      cows_path = Pathname(__FILE__).dirname.to_s + "/cows"
      env = {"COWPATH" => cows_path}
      IO.popen([env, perl_path, exec_path, message]) do |process|
        process.read.strip
      end
    end
  end
end
