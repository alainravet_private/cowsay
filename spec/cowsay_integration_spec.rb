require_relative '../lib/cow_app'
require 'rack/test'

set :environment, :test

describe 'The Cow App' do
  include Rack::Test::Methods

  def app
    CowApp
  end

  it "says hello by default" do
    expected = (<<'ASCIIART').strip
 _______ 
< hello >
 ------- 
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
ASCIIART

    get '/'
    result = last_response.body
    result.should == expected
  end

  
  it "says hello by default" do
    expected = (<<'ASCIIART').strip
 _______ 
< 12345 >
 ------- 
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
ASCIIART

    get '/?message=12345'
    result = last_response.body
    result.should == expected
  end
end

