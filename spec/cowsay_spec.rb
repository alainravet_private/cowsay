require_relative '../lib/cowsay'

describe Cowsay::Cow do

  it '#say' do
    expected = (<<'ASCIIART').strip
 _______ 
< hello >
 ------- 
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
ASCIIART

    result = subject.say("hello")
    result.should == expected
  end
end
